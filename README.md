### How to start? ###

* clone it
* npm install
* npm run dev

There is also a method to create prod version of the app

* npm run prod

###  Final thoughts ###
There are a few places where some improvements are needed :)
e.g. I should implement Bluebird.js promise library or use coroutines pattern.

