import './main.scss';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './app/app';
import AppReduxStore from "./app/app-redux-store";
import { Provider } from "react-redux";
import {
  BrowserRouter as Router,
  Switch
} from 'react-router-dom';

const Main = () => {
  return (
    <Provider store={AppReduxStore}>
      <Router>
        <Switch>
          <App />
        </Switch>
      </Router>
    </Provider>
  );
}

ReactDOM.render(<Main />, document.getElementById('main')); 