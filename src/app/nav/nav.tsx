import * as React from 'react';
import { ReactElement } from 'react';
import { RouteProps } from 'react-router';
import IAppState from '../i-app-state';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

const Nav = (props: RouteProps): ReactElement<RouteProps> => {
  let routes: object[] = [{ path: '/', label: 'Angular Repos user ranking' },
  { path: '/contributor-details', label: 'Contributor details' },
  { path: '/repo-details', label: 'Repo details' }];

  let generateLink = () => {
    let paths: object[] = routes.reduce((result: any, next: object) => {
      if (props.location.pathname.indexOf((next as any)['path']) > -1) {
        result = [...result, { ...next }];
      }
      return result;
    }, []);

    return paths.map((item: any, index: number) => {
      let isActive: boolean = index === (paths.length - 1);
      let style: string = `breadcrumb-item ${isActive ? 'active' : ''}`
      return <li key={index} className={style}>
        {isActive ? item.label : <Link to={item.path}>{item.label}</Link>}
      </li>
    })
  }

  return <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div className="container">
      <ol className="breadcrumb">
        {generateLink()}
      </ol>
    </div>
  </nav>
}
const mapStateToProps = (state: IAppState, ownProps: RouteProps) => ({ ...ownProps });
export default connect(mapStateToProps)(Nav);
