import { getRepoDetails } from './repo/repo-action';
import IRepo from './repo/i-repo';
import IContributor from './contributor/i-contributor';
import { getRepos } from './app-action';
import { filterBy } from './filter/filter-action';

export default interface IAppState {
  status: string;
  isLoading: boolean;
  isDetailsLoaded: boolean;
  repos: IRepo[];
  contributors: IContributor[];
  selectedContributor: IContributor;
  selectedRepo: IRepo;
  sortDir: number;
  getRepos?: Function;
  filterBy?: Function;
  getDetails?: Function;
  getRepoDetails?: Function;
}