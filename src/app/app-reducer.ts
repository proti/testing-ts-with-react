import { GET_REPO_DETAILS } from './repo/repo-action';
import IRepo from './repo/i-repo';
import IContributor from './contributor/i-contributor';
import { Reducer } from 'redux';
import IAppState from './i-app-state';
import IAction from './i-action';
import { DATA_LOADING, REPOS_DATA_READY, REPOS_JSON_LOADED } from './app-action';
import { FILTER_BY_CONTRIBUTIONS, FILTER_BY_FOLLOWERS, FILTER_BY_REPOS_AND_GISTS } from './filter/filter-action';
import { CONTRIBUTORS_DATA_READY, GET_CONTRIBUTOR_DETAILS } from './contributor/contributor-action';
import { flatten, removeDuplicates, sortBy } from './utils';

const initialState: IAppState = {
  status: '',
  isLoading: true,
  isDetailsLoaded: false,
  repos: [],
  contributors: [],
  selectedContributor: {} as IContributor,
  selectedRepo: {} as IRepo,
  sortDir: -1
};

const appReducer: Reducer<any> = (state: IAppState = initialState, action: IAction) => {
  let sortDir: number = (state.status === action.type) ? state.sortDir * -1 : state.sortDir;
  //shallow copy update - enough for this approach
  switch (action.type) {
    case DATA_LOADING:
      return {
        ...state,
        isLoading: true
      };
    case REPOS_JSON_LOADED:
      return {
        ...state,
        status: action.type,
        repos: action.payload
      };
    case REPOS_DATA_READY:
      return {
        ...state,
        status: action.type,
        repos: action.payload,
      };
    case CONTRIBUTORS_DATA_READY:
      return {
        ...state,
        status: action.type,
        isLoading: false,
        contributors: action.payload
      };
    case FILTER_BY_CONTRIBUTIONS:
      return {
        ...state,
        status: action.type,
        sortDir: sortDir,
        contributors: state.contributors.concat().sort(sortBy(sortDir, 'totalContributions'))
      };
    case FILTER_BY_FOLLOWERS:
      return {
        ...state,
        status: action.type,
        isDetailsLoaded: true,
        sortDir: sortDir,

        contributors: state.contributors.concat().sort(sortBy(sortDir, 'followers'))
      };
    case FILTER_BY_REPOS_AND_GISTS:
      return {
        ...state,
        status: action.type,
        isDetailsLoaded: true,
        sortDir: sortDir,

        contributors: state.contributors.concat().sort(sortBy(sortDir, 'publicRepos', 'gists'))
      };
    case GET_CONTRIBUTOR_DETAILS:
      return {
        ...state,
        selectedContributor: action.payload,
      };
    case GET_REPO_DETAILS:
      return {
        ...state,
        selectedRepo: state.repos.find(repo => repo.id === action.payload)
      };
    default:
      return {
        ...state,
        status: action.type
      };
  }
}
export default appReducer;