export default class AppRequestInit implements RequestInit {

  public method: string = 'GET';
  public headers: Headers = new Headers();
  public mode: RequestMode = 'cors';
  
  constructor() {
    this.headers.set('Content-Type', 'application/json');
    this.headers.append('Authorization', 'token 262070955f06859b296a6d1144e642d166f9356f');
  }
}