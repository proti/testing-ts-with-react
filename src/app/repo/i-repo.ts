import IContributor from '../contributor/i-contributor';

export default interface IRepo {
  id: string;
  name: string;
  description: string;
  contributorsUrl: string;
  contributors: IContributor[];
  forks: string;
}