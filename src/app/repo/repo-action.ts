import { getAction } from '../app-action';
export const GET_REPO_DETAILS: string = "GET_REPO_DETAILS";

export function getRepoDetails(repoId: string) {
  return (dispatch: Function) => {
    dispatch(getAction(GET_REPO_DETAILS, repoId));
  }
}