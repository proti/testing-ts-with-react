import './repo-details.scss';
import ContributorImg from '../contributor/contributor-img/contributor-img';
import * as React from 'react';
import { ReactElement } from 'react';
import IAppState from '../i-app-state';
import IContributor from '../contributor/i-contributor';
import IRepo from '../repo/i-repo';
import { RouteProps } from 'react-router';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Action, Dispatch } from 'redux';
import { getDetails } from '../contributor/contributor-action';

const RepoDetails = (props: IAppState & RouteProps): ReactElement<IAppState> => {
  let onContributorClicked = (id: string) => {
    props.getDetails(id);
    (props as any).history.push(`/contributor-details/:${id}`);
  };

  let showRepoContributors = () => {
    if (props.selectedRepo.contributors) {
      return props.selectedRepo.contributors.map((contributor: IContributor) => {
        return <li key={contributor.id} className="list-group-item list-group-item-action">
          <div className="user-thumb" onClick={() => onContributorClicked(contributor.id)}>
            <div className="col-sm-3 col-md-2">
              <ContributorImg {...contributor} />
            </div>
            <div className="col-sm-9 col-md-10">
              <h6 className="pt-1">{contributor.login}</h6>
              <div>contributions: {contributor.contributions}</div>
            </div>
          </div>
        </li>
      });
    }
  }

  let render = () => {
    let result = null;
    if (props.selectedRepo) {
      let repo: IRepo = props.selectedRepo;
      result = <div className="row">
        <div className="col-sm-12 col-md-6">
          <div className="card mb-4">
            <div className="card-block">
              <h3 className="card-title">Repo details</h3>
              <div className="col-sm-9 col-md-10">
                <h6 className="pt-1">{repo.name}</h6>
                <div>description: {repo.description}</div>
                <div>forks: {repo.forks}</div>
              </div>
            </div>
          </div>
        </div>

        <div className="col-sm-12 col-md-6">
          <div className="card mb-4">
            <div className="card-block">
              <h3 className="card-title">Contributors</h3>
              <ul className="list-group">{showRepoContributors()}</ul>
            </div>
          </div>
        </div>
      </div>
    }
    return result;
  }

  return render();
}

const mapStateToProps = (state: IAppState) => ({ ...state });
const mapDispatchToProps = (dispatch: Dispatch<Action>) => {
  return {
    getDetails: (payload: string) => dispatch(getDetails(payload))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(RepoDetails)
