import './app.scss';
import * as React from 'react';
import ContributorDetails from './contributor/contributor-details/contributor-details';
import RepoDetails from './repo/repo-details';
import ContributorsList from './contributor/contributor-list/contributors-list';
import { Dispatch, connect } from 'react-redux';
import { Action } from 'redux';
import IAppState from './i-app-state';
import { getRepos } from './app-action';
import { Route } from 'react-router-dom';
import Nav from './nav/nav';

class App extends React.Component<IAppState> {
  constructor(props: IAppState) {
    super(props);
  }

  componentDidMount() {
    this.props.getRepos()
  }

  render() {
    return (
      <div>
        <Route component={Nav} />
        <div className="container-fluid">
          <div className="row">
            <main className="col-sm-12 pt-3">
              <Route exact path="/" component={ContributorsList} />
              <Route path="/contributor-details" component={ContributorDetails} />
              <Route path="/repo-details" component={RepoDetails} />
            </main>
          </div>
        </div>
      </div >
    );
  }
}
 const mapDispatchToProps = (dispatch: Dispatch<Action>) => {
  return {
    getRepos: () => dispatch(getRepos())
  }
}
export default connect(null, mapDispatchToProps)(App as any);
