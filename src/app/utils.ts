import IContributor from './contributor/i-contributor';

export function flatten(array: any[], prop?: string) {
  return array.reduce((result: any, next: any) => result.concat(prop ? next[prop] : next), []);
}

export function removeDuplicates(from: any[], checkByParam?: string) {
  return from.filter((item: any, i: number, arr: any[]) => {
    return arr.findIndex((item2: any) => {
      let result: any = (item2 === item);
      if (checkByParam) {
        result = item2[checkByParam] === item[checkByParam];
      }
      return result;
    }) === i
  })
}

export function sortBy(dir: number, ...props: string[]) {
  return (a: any, b: any) => {
    for (let i: number = 0; i < props.length; i++) {
      return (a[props[i]] > b[props[i]]) ? -1 * dir : (a[props[i]] < b[props[i]]) ? 1 * dir : 0;
    };
  };
}
//removes falsy values from objtToClean
export function clean(objToClean: Object): Object | IContributor {
  return Object.keys(objToClean).reduce((obj, key) => {
    let prop = (<any>objToClean)[key];
    if (prop && !!prop) {
      return { ...obj, [key]: prop }
    } else if (isNumeric(prop)) {
      return { ...obj, [key]: 0 }
    }
    return obj
  }, {});
}

let isNumeric = (num: any) => !isNaN(parseFloat(num)) && isFinite(num);
