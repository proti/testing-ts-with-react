import { createStore, applyMiddleware } from 'redux';
import appReducer from './app-reducer';
import { logger } from 'redux-logger';
import thunk from 'redux-thunk';

export default createStore(appReducer, applyMiddleware(logger, thunk));