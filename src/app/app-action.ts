import { getContributors } from './contributor/contributor-action';
import AppRequestInit from './app-request-init';
import AppService from './app-service';

export const DATA_LOADING: string = 'DATA_LOADING';
export const REPOS_JSON_LOADED: string = 'REPOS_JSON_LOADED';
export const REPOS_DATA_READY: string = 'REPOS_DATA_READY';

export const getAction = (type: string, payload?: any) => ({ type, payload });

export function getRepos() {
  return (dispatch: Function) => {
    dispatch(getAction(DATA_LOADING));
    return fetch('https://api.github.com/users/angular/repos', new AppRequestInit())
      .catch(error => { throw new Error(error) })
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(response.statusText);
        }
      })
      .then(json => dispatch(getAction(REPOS_JSON_LOADED, AppService.getRepos(json))))
      .then(response => dispatch(getContributors()));
  }
}
