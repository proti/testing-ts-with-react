import { DATA_LOADING, getAction } from '../app-action';
import { loadUserData } from '../contributor/contributor-action';

export const FILTER_BY_CONTRIBUTIONS: string = "FILTER_BY_CONTRIBUTIONS";
export const FILTER_BY_FOLLOWERS: string = "FILTER_BY_FOLLOWERS";
export const FILTER_BY_REPOS_AND_GISTS: string = "FILTER_BY_REPOS_AND_GISTS";

export function filterBy(actionType: string) {
  return (dispatch: Function, getState: Function) => {

    if (actionType === FILTER_BY_CONTRIBUTIONS) {
      return dispatch(getAction(actionType));
    }

    if (!getState().isDetailsLoaded) {
      dispatch(getAction(DATA_LOADING));
      dispatch(loadUserData(actionType));
    } else {
      dispatch(getAction(actionType));
    };
  }
}