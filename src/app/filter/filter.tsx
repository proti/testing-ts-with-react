import * as React from 'react';
import { SyntheticEvent, ReactElement } from 'react';
import {
  FILTER_BY_CONTRIBUTIONS, FILTER_BY_FOLLOWERS,
  FILTER_BY_REPOS_AND_GISTS, filterBy
} from './filter-action';
import IAppState from '../i-app-state';
import { Action, Dispatch } from 'redux';
import { connect } from 'react-redux';

const Filter = (props: IAppState): ReactElement<IAppState> => {

  let onSelectChange = (event: SyntheticEvent<HTMLLabelElement>) => {
    props.filterBy(event.currentTarget.querySelector('input').value);
  }

  return (
    <div className="row">
      <div className="col-md-12">
        <div className="card mb-4">
          <div className="card-block">
            <div className="card-subtitle mb-2 text-muted">Filter by:</div>
            <div className="btn-group-sm" data-toggle="buttons">
              <label className="btn btn-danger" onClick={onSelectChange}>
                <input type="radio" name="options" value={FILTER_BY_CONTRIBUTIONS} />Contributions {props.sortDir > 0 ? '\u2193' : '\u2191'}</label>
              <label className="btn btn-danger" onClick={onSelectChange}>
                <input type="radio" name="options" value={FILTER_BY_FOLLOWERS} /> Followers {props.sortDir > 0 ? '\u2193' : '\u2191'}</label>
              <label className="btn btn-danger" onClick={onSelectChange}>
                <input type="radio" name="options" value={FILTER_BY_REPOS_AND_GISTS} /> Public repos & gists {props.sortDir > 0 ? '\u2193' : '\u2191'}</label>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
const mapsStateToProps = (state: IAppState) => ({ ...state })
const mapDispatchToProps = (dispatch: Dispatch<Action>) => {
  return {
    filterBy: (payload: string) => dispatch(filterBy(payload))
  }
}
export default connect(mapsStateToProps, mapDispatchToProps)(Filter);