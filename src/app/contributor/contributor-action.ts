import { FILTER_BY_CONTRIBUTIONS } from '../filter/filter-action';
import IContributor from './i-contributor';
import IRepo from '../repo/i-repo';
import IAppState from '../i-app-state';
import IAction from '../i-action';
import AppService from '../app-service';
import { getAction, REPOS_DATA_READY } from '../app-action';
import { flatten, removeDuplicates } from '../utils';

export const CONTRIBUTORS_DATA_READY: string = "CONTRIBUTORS_DATA_READY";
export const GET_CONTRIBUTOR_DETAILS: string = "GET_CONTRIBUTOR_DETAILS";

export function getContributors() {
  return (dispatch: Function, getState: Function) => {
    let repos: IRepo[] = (getState() as IAppState).repos;
    return Promise.all(repos.map((repo: IRepo) => AppService.getContributorsForRepo(repo)))
      .then((repos: IRepo[]) => dispatch(getAction(REPOS_DATA_READY, repos)))
      .then((action: IAction) => {
        //calculate totalContributions per user
        let contributors: Object[] = flatten(action.payload, 'contributors');
        let total: Object[] = contributors.reduce((result: any, next: IContributor) => {
          if (!result[next.id]) {
            result[next.id] = { ...next, totalContributions: next.contributions };
          } else {
            result[next.id].totalContributions += next.contributions;
          }
          return result;
        }, {});
        //convert Object to array
        let resultArray: Object[] = Object.keys(total).map((id: any) => total[id]);
        return dispatch(getAction(CONTRIBUTORS_DATA_READY, removeDuplicates(resultArray, 'id')));
      })
      .then(response => dispatch(getAction(FILTER_BY_CONTRIBUTIONS)))
      .catch(error => { throw new Error(error) });
  };
}

export function loadUserData(actionType: string) {
  return (dispatch: Function, getState: Function) => {
    let contributors: IContributor[] = getState().contributors;
    return Promise.all(contributors.map((user: IContributor) => AppService.getContributorDetails(user)))
      .then(response => dispatch(getAction(CONTRIBUTORS_DATA_READY, response)))
      .then(action => dispatch(getAction(actionType)))
      .catch(error => { throw new Error(error); });
  }
}

export function getDetails(id: string) {
  return (dispatch: Function, getState: Function) => {
    let state: IAppState = getState();
    let contributor: IContributor = state.contributors.find((user: IContributor) => user.id === id);
    contributor.contributedRepos = state.repos.filter((repo: any) => repo.contributors.filter((user: any) => user.id === id).length > 0);
    dispatch(getAction(GET_CONTRIBUTOR_DETAILS, contributor));
  }
}