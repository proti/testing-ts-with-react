import './contributor-img.scss';
import * as React from 'react';
import { ReactElement } from 'react';
import IContributor from '../i-contributor';

const ContributorImg = (props: IContributor) => {
  return <img src={props.avatarUrl} alt="profile photo" className="circle profile-photo" />
}
export default ContributorImg;