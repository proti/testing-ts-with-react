import ContributorImg from '../contributor-img/contributor-img';
import './contributor-details.scss';
import * as React from 'react';
import IRepo from '../../repo/i-repo';
import { ReactElement } from 'react';
import IAppState from '../../i-app-state';
import { RouteProps } from 'react-router';
import { Action, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { getRepoDetails } from '../../repo/repo-action';
import { Link } from 'react-router-dom';

const ContributorDetails = (props: IAppState & RouteProps): React.ReactElement<IAppState> => {

  let onRepoCicked = (id: string) => {
    props.getRepoDetails(id);
  }

  let showContributedRepos = () => {
    if (!props.selectedContributor.contributedRepos) return null;
    return props.selectedContributor.contributedRepos.map((repo: IRepo) => {
      return <li key={repo.id}>
        <Link to="/repo-details" className="nav-link" onClick={() => onRepoCicked(repo.id)}>{repo.name}</Link>
      </li>;
    });
  }
  let render = () => {
    let result = null;
    if (props.selectedContributor) {
      result = <div className="row">
        <div className="col-sm-12 col-md-6">
          <div className="card mb-4">
            <div className="card-block">
              <h3 className="card-title">Users details</h3>
              <div className="user-progress justify-center">
                <div className="col-sm-3 col-md-2">
                  <ContributorImg {...props.selectedContributor} />
                </div>
                <div className="col-sm-9 col-md-10">
                  <h6 className="pt-1">{props.selectedContributor.login}</h6>
                  <div>bio: {props.selectedContributor.bio}</div>
                  <div>totalContributions: {props.selectedContributor.totalContributions}</div>
                  <div>profile URL: <a href={props.selectedContributor.url}>{props.selectedContributor.url}</a></div>
                  <div>followers: {props.selectedContributor.followers}</div>
                  <div>public repos: {props.selectedContributor.publicRepos}</div>
                  <div>public gists: {props.selectedContributor.gists}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-sm-12 col-md-6">
          <div className="card mb-4"><div className="card-block">
            <h3 className="card-title">Contributed repos</h3>
            <ul>{showContributedRepos()}</ul>
          </div>
          </div>
        </div>
      </div>
    }
    return result
  }

  return render();

}
const mapStateToProps = (state: IAppState) => ({ ...state });
const mapDispatchToProps = (dispatch: Dispatch<Action>) => {
  return {
    getRepoDetails: (payload: string) => dispatch(getRepoDetails(payload))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ContributorDetails);