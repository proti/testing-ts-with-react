import IRepo from '../repo/i-repo';

export default interface IContributor {
  id: string;
  login: string;
  avatarUrl: string;
  contributions: number;
  totalContributions: number;
  url: string;
  followers: number;
  publicRepos: number;
  gists: number;
  bio: string;
  contributedRepos: IRepo[];
}