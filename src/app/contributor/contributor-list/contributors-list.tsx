import './contributors-list.scss';
import * as React from 'react';
import { ReactElement } from 'react';
import IAppState from '../../i-app-state';
import IContributor from './../i-contributor';
import { RouteProps } from 'react-router';
import { connect } from 'react-redux'
import { Action, Dispatch } from 'redux';
import { DATA_LOADING } from '../../app-action';
import { getDetails } from './../contributor-action';
import { Link } from 'react-router-dom';
import Filter from '../../filter/filter';

const ContributorsList = (props: IAppState & RouteProps): ReactElement<IAppState> => {

  let onContributorClicked = (id: string) => {
    props.getDetails(id);
    (props as any).history.push(`/contributor-details/:${id}`);
  }

  let showlist = () => {
    return props.contributors.map((contributor: IContributor, index: number) => {
      let id: string = contributor.id;
      return (<tr key={id} onClick={() => onContributorClicked(id)}>
        <td>{index}</td>
        <td>{contributor.login}</td>
        <td><Link to="/contributor-details" className="nav-link">{contributor.totalContributions}</Link></td>
        <td>{contributor.followers}</td>
        <td>{contributor.publicRepos}</td>
        <td>{contributor.gists}</td>
      </tr>)
    });
  }

  let render = () => {
    if (props.isLoading) {
      return (<div>
        Fetching data...
        </div>)
    } else {
      return (<div>
        <Filter {...props} />
        <div className="row">
          <div className="col-md-12">
            <div className="card mb-4">
              <div className="card-block">
                <h3 className="card-title">Ranking</h3>
                <div className="table-responsive">
                  <table className="table table-striped table-hover">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Login</th>
                        <th>Contributions</th>
                        <th>Followers</th>
                        <th>Public repos</th>
                        <th>Public gists</th>
                      </tr>
                    </thead>
                    <tbody>
                      {showlist()}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>)
    }
  }

  return render();
}
const mapStateToProps = (state: IAppState) => ({ ...state });
const mapDispatchToProps = (dispatch: Dispatch<Action>) => {
  return {
    getDetails: (payload: string) => dispatch(getDetails(payload))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ContributorsList)