import IRepo from './repo/i-repo';
import IContributor from './contributor/i-contributor';
import AppRequestInit from './app-request-init';
import { clean } from './utils';

export default class AppService {

  static getRepos(loadedData: Object[]): IRepo[] {
    return loadedData.map((data: Object) => {
      let dataTyped: any = (<any>data);
      let repo: IRepo = {} as IRepo;
      repo.id = dataTyped['id'];
      repo.name = dataTyped['name'];
      repo.contributorsUrl = dataTyped['contributors_url'];
      repo.description = dataTyped['description'];
      repo.forks = dataTyped['forks'];
      return clean(repo) as IRepo;
    });
  }

  static getContributorsForRepo(repo: IRepo): Promise<IRepo> {
    return (fetch(repo.contributorsUrl, new AppRequestInit())
      .then(data => data.json())
      .then(response => {
        return {
          ...repo,
          contributors: response.map((item: object) => AppService.getContributor(item))
        } as IRepo;
      })
      .catch(error => { throw new Error(error) })
    );
  }

  static getContributor(data: Object): IContributor {
    let dataTyped: any = (<any>data);
    let contributor: IContributor = {} as IContributor;
    contributor.id = dataTyped['id'];
    contributor.login = dataTyped['login'];
    contributor.avatarUrl = dataTyped['avatar_url'];
    contributor.url = dataTyped['url'];
    contributor.contributions = dataTyped['contributions'];
    contributor.bio = dataTyped['bio'];
    contributor.followers = dataTyped['followers'];
    contributor.publicRepos = dataTyped['public_repos'];
    contributor.gists = dataTyped['public_gists'];

    return clean(contributor) as IContributor;
  }

  static getContributorDetails(contributor: IContributor): Promise<IContributor> {
    return (fetch(contributor.url, new AppRequestInit())
      .then(data => data.json())
      .then(response => {
        let newContr: IContributor = AppService.getContributor(response);
        return { ...contributor, ...newContr } as IContributor;
      })
      .catch(error => { throw new Error(error) })
    );
  }
}