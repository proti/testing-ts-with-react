const path = require("path");
const SRC_DIR = path.join(__dirname, "src");
const DIST_DIR = path.join(__dirname, "dist");

const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const glob = require('glob');
const PurifyCSSPlugin = require('purifycss-webpack');
const webpack = require('webpack');
//const CopyWebpackPlugin = require('copy-webpack-plugin');

const isProd = process.env.NODE_ENV === 'prod';
const cssDev = ['style-loader', 'css-loader', 'sass-loader'];
const cssProd = ExtractTextPlugin.extract({
    fallback: 'style-loader',
    use: ['css-loader', 'sass-loader']
});
const cssConfig = isProd ? cssProd : cssDev;

//make website public for easy testing on mobile devices it is not really secure though.
//TODO: defined for Mac only for now, need to add testing the OS
//const localhost = require('os').networkInterfaces().en0[1].address;
const localhost = 'localhost';

const config = {
    entry: {
        app: SRC_DIR + "/main.tsx",
        bootstrap: "./node_modules/bootstrap",
        jquery: "./node_modules/jquery"
    },
    output: {
        path: DIST_DIR,
        filename: "js/[name].bundle.js"
    },
    devtool: "source-map",
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            },
            {
                test: /\.scss$/,
                use: cssConfig
            },
            {
                test: /.*\.(gif|png|jpe?g|svg)$/i,
                exclude: /(\/fonts)/,
                use: 'file-loader?name=[name].[ext]&outputPath=images/&publicPath=../'
            },
            {
                test: /\.woff$/,
                use: 'file-loader?limit=65000&mimetype=application/font-woff&name=fonts/[name].[ext]'
            },
            {
                test: /\.woff2$/,
                use: 'file-loader?limit=65000&mimetype=application/font-woff2&name=fonts/[name].[ext]'
            },
            {
                test: /\.[ot]tf$/,
                use: 'file-loader?limit=65000&mimetype=application/octet-stream&name=fonts/[name].[ext]'
            },
            {
                test: /\.eot$/,
                use: 'file-loader?limit=65000&mimetype=application/vnd.ms-fontobject&name=fonts/[name].[ext]'
            }
        ]
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js"]
    },
    devServer: {
        contentBase: DIST_DIR,
        compress: true,
        public: localhost + ':8080',
        host: '0.0.0.0',
        port: 8080,
        stats: 'minimal',
        open: true,
        openPage: "",
        historyApiFallback: true
    },
    plugins: [
        //To have bootstrap fully working 
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            Popper: ['popper.js', 'default']
        }),
        new HtmlWebpackPlugin({
            minify: {
                collapseWhitespace: true
            },
            hash: true,
            template: './src/index.html'
        }),
        new ExtractTextPlugin({
            filename: 'css/[name].css',
            disable: !isProd,
            allChunks: true
        }),
        new PurifyCSSPlugin({
            paths: glob.sync(path.join(__dirname, 'src/*.html')),
            minimize: true
        }),
        //Copy files from src/data to dist/data e.g. json files
        /*new CopyWebpackPlugin([
            {from: SRC_DIR + '/data', to: DIST_DIR + '/data'}
        ])*/ 
    ]
};

module.exports = config;